#!/usr/bin/env python
# coding: utf-8



import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns
import glob
import os
import warnings
warnings.filterwarnings('ignore')

import sklearn


df = pd.read_csv('data/data_test3.csv') 


df['SoftSkill_Cleaver'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')


def clean_data(x):
  
  if isinstance(x, str):
      return str.lower(x.replace(" ", "").replace(",", " "))
  else:
      return ''

columnas = ["HardSkill_English"]

for c in columnas:
  df[c] = df[c].apply(clean_data)

def create_soup(df):
  return "".join(df['HardSkill_English']) 

df["tecniques_skill"] = df.apply(create_soup, axis=1)
#df["tecniques_skill"]

copia_df = df.copy()

#copia_df

count_vectorizer = CountVectorizer(stop_words='english')
count_vectorizer = CountVectorizer()
sparse_matrix = count_vectorizer.fit_transform(df.tecniques_skill)

indices = pd.Series(df['Title'], index=df.index)
#indices[0]

doc_term_matrix = sparse_matrix.todense()
df_skill = pd.DataFrame(doc_term_matrix, 
                  columns=count_vectorizer.get_feature_names_out(), 
                  index=list(df.index.values))
#df_skill

from sklearn.metrics.pairwise import cosine_similarity
cosine_similarity(df_skill, df_skill)

def obtenerRecomendacion_hs(skill):
  count_vectorizer = CountVectorizer(stop_words='english')
  count_vectorizer = CountVectorizer()
  #Empleos
  sparse_matrix = count_vectorizer.fit_transform(df.tecniques_skill)
  doc_term_matrix = sparse_matrix.todense()
  df_skill = pd.DataFrame(doc_term_matrix, 
                    columns=count_vectorizer.get_feature_names_out(), 
                    index=list(df.index.values))
  #Usuario
  #usuario = count_vectorizer.fit_transform(["swift scrum devops git graphql"])
  usuario_sparse = count_vectorizer.fit_transform([skill])
  usuario_matriz = usuario_sparse.todense()
  usuario_fila = pd.DataFrame(usuario_matriz, 
                    columns=count_vectorizer.get_feature_names_out())
  #df vacio con las mismas columnas que df_skill
  df_usuario = pd.DataFrame(None, columns=df_skill.columns)
  #se añade la fila con los datos del usuario
  df_usuario = df_usuario.append(usuario_fila)
  #se colocan en 0 las habilidades que no tiene el usuario
  df_usuario = df_usuario.fillna(0)
  #Cosine similarity
  vector_usu = np.array(df_usuario.loc[0, :]).reshape((1, -1))
  indices = list(df.index.values)
  title = list(df.Title)
  lista_Con= []
  for i in (indices):
    lista_Con.append(cosine_similarity(vector_usu, np.array(df_skill.loc[i, :]).reshape((1, -1)))[0][0]) 
  list_of_tuples = list(zip(indices, title, lista_Con))
  result = pd.DataFrame(list_of_tuples, columns = ['Indice', 'Title', 'Cosine'])
  result = result[result['Cosine'] >= .60]

  result["Porcentaje"] = result["Cosine"] *100

  result = result.sort_values(by='Cosine', ascending=False)

  result = result.iloc[0:15]
  return result




obtenerRecomendacion_hs("c1 html css python django")


obtenerRecomendacion_hs("c2 html css react")


obtenerRecomendacion_hs("c1")





import plotly.express as px
import pandas as pd

df = pd.DataFrame(dict(
    r=[1, 0.80, 0.44],
    theta=['English','Hard Skills','Soft Skills']))

fig = px.line_polar(df, r='r', theta='theta', line_close=True)

fig.show()


# In[ ]:

